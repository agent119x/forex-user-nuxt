/*=========================================================================================
  File Name: axiosInstance.js
  Description: Axios Instance
  ----------------------------------------------------------------------------------------
  Author: John Arvin Nuarin
==========================================================================================*/

import axios from 'axios';
import Cookie from 'cookie-universal';

const cookies = Cookie();

let token = null;

try {
  token = `Bearer ${ cookies.get('access_token') }`;
} catch(e) {f
  token = null;
}

let config = {}

if (process.browser) {
  if(window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1') {
    config = {
      baseURL: 'http://api.coincityfx.test/api/customer',
      timeout: 10000,
      headers: {'Content-Type': 'application/json', 'Accept': 'application/json', 
      'X-Requested-With': 'XMLHttpRequest', 'Authorization': token}
    }
  } else {
   config = {
      baseURL: 'https://staging.coincityfx.com/api/customer',
      timeout: 10000,
      headers: {'Content-Type': 'application/json', 'Accept': 'application/json', 
      'X-Requested-With': 'XMLHttpRequest', 'Authorization': token}
    }
  }
}

const ax = axios.create(config);
export default ax;