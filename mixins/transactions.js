import ax from '@/axiosInstance';

export default {
    methods: {
        async callApi(method, url, dataObj) {
            try {
                let response = await ax({
                    method: method,
                    url: url,
                    data: dataObj
                });
                return response.data;
            } catch (e) {
                return e.response.data;
            }
        },
        getWallets(wallets) {
            wallets = wallets.map((wallet) => {
                return {
                    value: wallet.wallet_id, //Needed for form-select
                    text: wallet.currency,  //Needed for form-select
                    wallet_id: wallet.wallet_id,
                    balance: wallet.balance
                }
            });
        
            wallets.unshift({
                value: null,
                text: this.$t('transactions.pleaseSelect'),
                wallet_id: null,
                balance: 0
            });
            return wallets;
        },
        getType(value) {
            let type;
            value = value || "";
            if ((value === 1) || (value.toString().toLowerCase() === "deposit")) {
                type = this.$t('transactions.deposit')
            } else if ((value === 2) || (value.toString().toLowerCase() === "withdraw")) {
                type = this.$t('transactions.withdrawal')
            }
            return type;
        },
        getStatus(value) {
            let status;
            if (value === 1) {
                status = this.$t('transactions.pending')
            } else if (value === 2) {
            status = this.$t('transactions.processing')
            } else if (value === 3) {
                status = this.$t('transactions.completed')
            } else if (value === 4) {
                status = this.$t('transactions.rejected')
            }
            return status;
        },
        downloadCSV(items, filename) {
            if (items.length > 0) {
                let firstItem = items[0];
                let headers = this.$_.allKeys(firstItem);
                let csv = "";

                csv += headers.join(",");
                csv += "\n";

                this.$_.each(items, (item) => {
                    csv += this.$_.values(item).join(",");
                    csv += "\n";
                });

                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                hiddenElement.target = '_blank';
                hiddenElement.download = filename + ".csv";
                hiddenElement.click();
            }
        }
    }
  }