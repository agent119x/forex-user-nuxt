import state from './i18nState';
import actions from './i18nActions';
import getters from './i18nGetters';
import mutations from './i18nMutations';

export default {
  state,
  actions,
  mutations,
  getters
}