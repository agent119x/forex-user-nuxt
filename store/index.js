import Vuex from 'vuex';
import walletModule from './wallet/index'
import i18nModule from './i18n/index'

const createStore = () => {
  return new Vuex.Store({
    namespaced: true,
    modules: {
      i18n: i18nModule,
      wallet: walletModule,
    }
  });
};

export default createStore