export default {
    SET_WALLETS (state, wallets) {
        state.wallets = wallets;
    },
    SET_DEFAULT_WALLET (state, wallet) {
      if (Object.keys(state.selectedWallet).length == 0) {
        state.selectedWallet = wallet;
      }
    },
    SET_SELECTED_WALLET (state, wallet) {
      state.selectedWallet = wallet;
    }
}