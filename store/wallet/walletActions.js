import ax from '@/axiosInstance'

export default {
    async GET_WALLETS ({commit}) {
        await ax.get('wallets').then(res => {
            commit('SET_WALLETS', res.data);
            commit('SET_DEFAULT_WALLET',res.data[0])
        })
    },
    SET_SELECTED_WALLET({commit}, wallet) {
        commit('SET_SELECTED_WALLET', wallet);
    }
}