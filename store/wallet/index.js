import state from './walletState';
import actions from './walletActions';
import getters from './walletGetters';
import mutations from './walletMutations';

export default {
  state,
  actions,
  mutations,
  getters
}