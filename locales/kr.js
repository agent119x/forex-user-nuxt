export default {
    user: {
        profile: "프로필",
        wallets: "지갑",
        settings: "설정"
    },
    navbar: {
        exchange: "교환",
        deposits: "매장",
        withdrawals: "인출",
        transactions: "업무",
        wallets: "지갑",
        myWallet: "내 지갑",
        profile: "프로필",
        settings: "설정",
        logOut: "로그 아웃"
    },    
    exchange: {
        orderBook: "Order Book",
        priceBTC: "Price(BTC)",
        amountETH: "Amount(ETH)",
        totalETH: "Total(ETH)",
        limit: "Limit",
        market: "Market",
        stopLimit: "Stop Limit",
        stopMarket: "Stop Market",
        price: "Price",
        amount: "Amount",
        available: "Available",
        volume: "Volume",
        margin: "Margin",
        fee: "Fee",
        recentTrades: "Recent Trades",
        marketDepth: "Market Depth",
        time: "Time",
        openOrders: "Open Orders",
        closedOrders: "Closed Orders",
        orderHistory: "Order History",
        balance: "Balance",
        noData: "No Data",
        buy: "Buy",
        sell: "Sell"
    }
}
  