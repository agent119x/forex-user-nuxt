export default {
    //Navbar
    navbar: {
        exchange: "Exchange",
        deposits: "Deposits",
        withdrawals: "Withdrawals",
        transactions: "Transactions",
        wallets: "Wallets",
        myWallet: "My Wallet",
        profile: "Profile",
        settings: "Settings",
        logOut: "Log Out"
    }, 
    //User Page Translations
    user: {
        profile: "Profile",
        wallets: "Wallets",
        settings: "Settings"
    },   
    //Exchange Page Translations
    exchange: {
        orderBook: "Order Book",
        priceBTC: "Price(BTC)",
        amountETH: "Amount(ETH)",
        totalETH: "Total(ETH)",
        direct: "Direct",
        stopLimit: "Stop Limit",
        price: "Price",
        amount: "Amount",
        available: "Available",
        volume: "Volume",
        margin: "Margin",
        fee: "Fee",
        recentTrades: "Recent Trades",
        marketDepth: "Market Depth",
        time: "Time",
        openOrders: "Open Orders",
        closedOrders: "Closed Orders",
        orderHistory: "Order History",
        balance: "Balance",
        noData: "No Data",
        buy: "Buy",
        sell: "Sell",
        time: "Time",
        allPairs: "All Pairs",
        allTypes: "All Types",
        buySell: "Buy/Sell",
        executed: "Executed",
        unexecuted: "Unexecuted"
    },
    transactions: {
        pleaseSelect: "Please select currency",
        date: "Date",
        currency: "Currency",
        amount: "Amount",
        mop: "MOP",
        bank: "Bank",
        accountName: "Account Name",
        accountNumber: "Account Number",
        status: "Status",
        type: "Type",
        deposit: "Deposit",
        depositPlural: "Deposits",
        withdrawal: "Withdrawal",
        withdrawalPlural: "Withdrawals",
        pending: "Pending",
        processing: "Processing",
        completed: "Completed",
        rejected: "Rejected",
        availableBalance: "Available Balance",
        seeAllTransactions: "See All Transactions",
        pleaseWait: "Please wait while your transaction is going...",
        exportCSV: "Export as CSV",
        bankTransfer: "Bank Transfer",
        blockchain: "Blockhain",
        depositTrans: "Deposit Transactions"
    }

}
  