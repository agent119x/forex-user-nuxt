import Vue from 'vue';
import VueI18n from 'vue-i18n';

import TranslationDataEN from '~/locales/en.js';
import TranslationDataKR from '~/locales/kr.js';
import TranslationDataZH from '~/locales/zh.js';

Vue.use(VueI18n);

export default ({ app, store }) => {
  app.i18n = new VueI18n({
    locale: store.state.i18n.locale,
    fallbackLocale: 'en',//always displays English if other translation is not available
    messages: {
      //'locales' directory contains all the translations
      'en': TranslationDataEN,
      'kr': TranslationDataKR,
      'zh': TranslationDataZH
    }
  })
}
